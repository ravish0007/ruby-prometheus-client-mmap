require 'rspec/core/rake_task'

desc 'Default: run specs'
task default: [:spec]

# test alias
task test: :spec

desc 'Run specs'
RSpec::Core::RakeTask.new do |t|
  t.pattern = FileList['spec/**/*_spec.rb']
  t.rspec_opts = '--require ./spec/spec_helper.rb'
end

